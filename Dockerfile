FROM maven
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
