package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	scannercli "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/cli"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
)

const (
	mavenPluginVersion  = "0.3.0"
	mavenPluginArtifact = "com.gemnasium:gemnasium-maven-plugin:" + mavenPluginVersion
	mavenPluginOutput   = "gemnasium-maven-plugin.json"
)

func analyzeFlags() []cli.Flag {
	return scannercli.ClientFlags()
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	// parse args and make new client
	client, err := scannercli.NewClient(c)
	if err != nil {
		return nil, err
	}

	// install gemnasium-maven-plugin
	install := setupCmd(exec.Command("mvn", "org.apache.maven.plugins:maven-dependency-plugin:get", "-Dartifact="+mavenPluginArtifact))
	if err := install.Run(); err != nil {
		return nil, err
	}

	// build project to make sure internal deps are built
	build := setupCmd(exec.Command("mvn", "install", "-DskipTests"))
	if err := build.Run(); err != nil {
		return nil, err
	}

	// run gemnasium-maven-plugin
	run := setupCmd(exec.Command("mvn", "com.gemnasium:gemnasium-maven-plugin:dump-dependencies"))
	if err := run.Run(); err != nil {
		return nil, err
	}

	// Walk the project to find all files
	files, err := getDependencyFiles(path)
	if err != nil {
		return nil, err
	}

	var sources scanner.Sources
	for _, file := range files {
		// parse file generated by the gemnasium-maven-plugin
		input, err := os.Open(file)
		if err != nil {
			return nil, err
		}
		defer input.Close()

		// get the path to the module's pom.xml (in case we have multi-modules)
		relativeFilePath, err := filepath.Rel(path, file)
		if err != nil {
			return nil, err
		}
		pomFilePath := filepath.Join(filepath.Dir(relativeFilePath), "pom.xml")

		source, err := scanner.Parse(input, "maven-dependencies.json", pomFilePath)
		if err != nil {
			return nil, err
		}
		sources = append(sources, *source)
	}

	// get advisories
	advisories, err := client.Advisories(sources.Packages())
	if err != nil {
		return nil, err
	}

	// return affected sources
	var output bytes.Buffer
	result := scanner.AffectedSources(advisories, sources...)
	enc := json.NewEncoder(&output)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}

func getDependencyFiles(path string) ([]string, error) {
	var matches []string

	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.Name() == mavenPluginOutput {
			matches = append(matches, path)
		}

		return nil
	})

	return matches, err
}
